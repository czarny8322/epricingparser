﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HtmlWebParser.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("HtmlWebParser.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;html&gt;
        ///&lt;head&gt;
        ///&lt;style type=&quot;text/css&quot;&gt;
        ///
        /// p.MsoNormal
        ///	{margin-bottom:.0001pt;
        ///	font-size:10.0pt;
        ///	font-family:&quot;Calibri&quot;,sans-serif;
        ///	margin-left: 0cm;
        ///	margin-right: 0cm;
        ///	margin-top: 0cm;
        ///}
        /// table.MsoNormalTable
        ///	{font-size:10.0pt;
        ///	font-family:&quot;Times New Roman&quot;,serif;
        ///}
        ///&lt;/style&gt;
        ///&lt;/head&gt;
        ///
        ///&lt;body&gt;
        ///[template]
        ///&lt;table bordercolor=&quot;#A0A0A0&quot; background=&quot;#A0A0A0&quot; width=&quot;100%&quot; border=&quot;1&quot; solid=&quot;black&quot; border-collapse=&quot;collapse&quot; cellpadding=&quot;0&quot; cellspacing=&quot;1&quot; class=&quot;MsoNormalTable&quot; &gt;
        ///	
        ///	&lt;tr&gt;
        /// [rest of string was truncated]&quot;;.
        /// </summary>
        public static string MCI_template {
            get {
                return ResourceManager.GetString("MCI_template", resourceCulture);
            }
        }
    }
}
