﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HtmlWebParser.Extensions
{

    public static class MyExtensions
    {
        public static IList<T> SwapIndex<T>(this IList<T> source, int index1, int index2)
        {
            // Parameter checking is skipped in this example.

            // If nothing needs to be swapped, just return the original collection.
            if (index1 == index2)
                return source;

            // Make a copy.
            List<T> copy = source.ToList();

            // Swap the items.
            T temp = copy[index1];
            copy[index1] = copy[index2];
            copy[index2] = temp;

            // Return the copy with the swapped items.
            return copy;
        }

        public static String ClearString(this String source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                if (source.Contains("&nbsp;")){ source = source.Replace("&nbsp;", ""); };
                if (source.Contains("\r\n")) { source = source.Replace("\r\n", ""); };
                if (source.Contains("\t")) { source = source.Replace("\t", ""); };
            }
            return source;
        }
    }
}
