﻿using HtmlWebParser.Helpers;
using Microsoft.Owin;
using Owin;
using System;
using TelemetrySdk.Helpers;
using TelemetrySdk.Models;

[assembly: OwinStartupAttribute(typeof(HtmlWebParser.Startup))]
namespace HtmlWebParser
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            
            try
            {
                //initialise telemetry 
                // 1) send To API interval [sec], 2) Max batch logs size (optional - 30 default), 5) action log (optional/null default)
                TelemetryHelper.Instance.RegisterProduct(TeleHelper.telemetryClientId,
                    new ClientMetadata
                    {
                        ProductId = 12, // request for productId
                        LogAction = new Action<Exception>((ex) => {
                            Console.WriteLine(ex.ToString());
                        }), // optional
                        ProcustVersion = "1.0.0.0"
                    }
                );
                //TelemetryHelper.Instance.SetCurrentPcc = getCurrentPccFunc;
                //TelemetryHelper.Instance.ClientId = GetClientId();
                TelemetryHelper.Instance.LogLaunchEvent(TeleHelper.telemetryClientId);
                //TeleHelper.LogCustomObject(tryParse);
                TelemetryHelper.Instance.SaveAllAndClose();

                //TelemetryHelper.Instance.LogCustomObject(telemetryClientId, new { Avail_Data = new List<String> { "test", "test1" } }, "runPluginData");
                //TelemetryHelper.Instance.LogException(telemetryClientId, $"Exception message {i}...");
            }
            catch (Exception e)
            {

                throw;
            }


        }
    }
}
