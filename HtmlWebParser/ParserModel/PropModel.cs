﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlWebParser.ParserModel
{
    public class PropModel
    {
        public string tdEmailAirline { get; set; }
        public string tdEmailAirlineCode { get; set; }
        public string lblEmailFlightNumber { get; set; }
        public string tdEmailBookingCode { get; set; }
        public string tdEmailDepartureDate { get; set; }
        public string lblEmailDepartureCity { get; set; }
        public string tdEmailDepartureTime { get; set; }
        public string tdEmailArrivalTime { get; set; }
        public string lblEmailArrivalCity { get; set; }
        public string tdEmailEquipCode { get; set; }
        public string tdEmailStops { get; set; }
        public string tdEmailOperatedBy { get; set; }       
        
    }
}
