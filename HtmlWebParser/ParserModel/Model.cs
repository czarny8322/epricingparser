﻿using System.Collections.Generic;

namespace HtmlWebParser.ParserModel
{
    public class Model
    {        
        public string lblEmailTotPrice { get; set; }
        public string tdEmailCurrencyCode { get; set; }
        public List<PropModel> propModel = new List<PropModel>();
    }
    
}
