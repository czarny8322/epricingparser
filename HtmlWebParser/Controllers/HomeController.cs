﻿using HtmlWebParser.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelemetrySdk.Helpers;
using TelemetrySdk.Models;

namespace HtmlWebParser.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? ID)
        {
            //var _base64 = string.Empty;

            //var isFormData = Request.Form.Count > 0;          
            //if (isFormData)
            //{
            //    var base64 = Request.Form[0];
            //    var html = Base64Decode(base64);
            //    var htmlAfterParse = htmlParser.TryParse(html);
            //    _base64 = Base64Encode(htmlAfterParse);                
            //}
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ajaxCall(string webValue)
        {
            
            var tryParse = htmlParser.TryParse(webValue);
            var _base64 = Base64Encode(tryParse);


            try
            {
                //initialise telemetry 
                // 1) send To API interval [sec], 2) Max batch logs size (optional - 30 default), 5) action log (optional/null default)
                TelemetryHelper.Instance.RegisterProduct(TeleHelper.telemetryClientId,
                    new ClientMetadata
                    {
                        ProductId = 12, // request for productId
                    LogAction = new Action<Exception>((ex) => {
                        Console.WriteLine(ex.ToString());
                    }), // optional
                    ProcustVersion = "1.0.0.0"
                    }
                );
                //TelemetryHelper.Instance.SetCurrentPcc = getCurrentPccFunc;
                //TelemetryHelper.Instance.ClientId = GetClientId();
                TelemetryHelper.Instance.LogLaunchEvent(TeleHelper.telemetryClientId);
                TeleHelper.LogCustomObject(tryParse);
                TelemetryHelper.Instance.SaveAllAndClose();

                //TelemetryHelper.Instance.LogCustomObject(telemetryClientId, new { Avail_Data = new List<String> { "test", "test1" } }, "runPluginData");
                //TelemetryHelper.Instance.LogException(telemetryClientId, $"Exception message {i}...");
            }
            catch (Exception e)
            {

                throw;
            }


            return Content(_base64);            
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

    }
}