﻿using HtmlAgilityPack;
using HtmlWebParser.Extensions;
using HtmlWebParser.ParserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace HtmlWebParser
{
    public class htmlParser
    {
        public htmlParser()
        {
           
        }

        public static string TryParse(string html)
        {
            var newItinerary = string.Empty;

            if (!string.IsNullOrEmpty(html))
            {
                List<Model> prices = new List<Model>();
               
                HtmlDocument someNode = new HtmlDocument();
                someNode.LoadHtml(html);
                
                if (someNode != null)
                {                    
                    List<List<HtmlNode>> listAfterSplit = new List<List<HtmlNode>>();
                    var getBodyToParse = someNode.GetElementbyId("EmailAlternate");
                    
                    if(getBodyToParse != null && getBodyToParse.InnerHtml.Contains("lblEmailTotPrice"))
                    {
                        listAfterSplit = SplitMyHtmlBy(getBodyToParse, "lblEmailTotPrice", "tr");
                    }

                    List<string> contentInHtml = changeHtmlNodeToListString(listAfterSplit);

                    if (contentInHtml.Count() > 0)


                        foreach (var section in contentInHtml)
                        {
                            HtmlDocument html_Section = new HtmlDocument();
                            html_Section.LoadHtml(section);

                            var priceSection = htmlParse(html_Section);

                            prices.Add(priceSection);
                        }
                }

                var stringTemplate = Properties.Resources.MCI_template;
               
                newItinerary = CreateNewItinerary(stringTemplate, prices);                
            }
            return newItinerary;
        }

        private static List<List<HtmlNode>> SplitMyHtmlBy(HtmlNode getBodyToParse, string splitByString, string descendasAs)
        {
            List<HtmlNode> myNodeList = new List<HtmlNode>();
            List<List<HtmlNode>> listAfterSplit = new List<List<HtmlNode>>();           
                        
            var trList = getBodyToParse.Descendants(descendasAs);
            
            bool storeData = true;
            int index = 0;
            foreach (HtmlNode tr in trList)
            {
                storeData = tr.InnerHtml.Contains(splitByString);
                if (storeData)
                {
                    if (myNodeList.Count != 0)
                    {
                        listAfterSplit.Add(myNodeList);
                        myNodeList = new List<HtmlNode>();
                    }
                }
                myNodeList.Add(tr);

                index++;
                if (index == trList.Count())
                {
                    listAfterSplit.Add(myNodeList);
                }
            }
            return listAfterSplit;
        }



        private static List<string> changeHtmlNodeToListString(List<List<HtmlNode>> listAfterSplit)
        {
            List<string> myStringTable = new List<string>();

            foreach (List<HtmlNode> i in listAfterSplit)
            {
                string myHTML = "";
                foreach (HtmlNode x in i)
                {
                    myHTML += x.InnerHtml;
                }
                myStringTable.Add(myHTML);
            }
            return myStringTable;
        }

        private static string CreateNewItinerary(string stringTemplate, List<Model> ListDatafromItinerary)
        {
            string readHtmlTemplate = stringTemplate;
            //string readHtmlTemplate = File.ReadAllText(path);
            var bodyHTML = Between(readHtmlTemplate, "[template]", "[/template]");
            var itineraryHtmlSection = Between(readHtmlTemplate, "[itinerary]", "[/itinerary]");
            
            var tepmplateAfterParse = string.Empty;

            if (ListDatafromItinerary.Count > 0)
            {
                List<string> tableList = new List<string>();

                int i = 1;
                foreach (var offer in ListDatafromItinerary)
                {
                    var _itinerraryTemplate = bodyHTML.Replace("%OptionNumber%", i.ToString());
                    i++;

                    List<string> listItineraryHtmlSection = new List<string>();
                    foreach (var item in offer.propModel)
                    {
                        var _itineraryHtmlSection = itineraryHtmlSection;
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%AirlineImage%")) ? _itineraryHtmlSection.Replace("%AirlineImage%", string.Format("<img src=\"{0}\" alt=\"Mountain View\">", item.tdEmailAirline)) : _itineraryHtmlSection.Replace("%AirlineImage%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%FlightNumber%")) ? _itineraryHtmlSection.Replace("%FlightNumber%", item.lblEmailFlightNumber) : _itineraryHtmlSection.Replace("%FlightNumber%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Class%")) ? _itineraryHtmlSection.Replace("%Class%", item.tdEmailBookingCode) : _itineraryHtmlSection.Replace("%Class%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%From%")) ? _itineraryHtmlSection.Replace("%From%", item.lblEmailDepartureCity) : _itineraryHtmlSection.Replace("%From%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Destination%")) ? _itineraryHtmlSection.Replace("%Destination%", item.lblEmailArrivalCity) : _itineraryHtmlSection.Replace("%Destination%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Leaving%")) ? _itineraryHtmlSection.Replace("%Leaving%", item.tdEmailDepartureTime) : _itineraryHtmlSection.Replace("%Leaving%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%DepartureDate%")) ? _itineraryHtmlSection.Replace("%DepartureDate%", item.tdEmailDepartureDate) : _itineraryHtmlSection.Replace("%DepartureDate%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Arriving%")) ? _itineraryHtmlSection.Replace("%Arriving%", item.tdEmailArrivalTime) : _itineraryHtmlSection.Replace("%Arriving%", "");
                        // _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Durration%")) ? _itineraryHtmlSection.Replace("%Durration%", item.) : "";
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%PlaneType%")) ? _itineraryHtmlSection.Replace("%PlaneType%", item.tdEmailEquipCode) : _itineraryHtmlSection.Replace("%PlaneType%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%OperateBy%")) ? _itineraryHtmlSection.Replace("%OperateBy%", item.tdEmailOperatedBy) : _itineraryHtmlSection.Replace("%OperateBy%", "");
                        _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%Stops%")) ? _itineraryHtmlSection.Replace("%Stops%", item.tdEmailStops) : _itineraryHtmlSection.Replace("%Stops%", "");
                        // _itineraryHtmlSection = (_itineraryHtmlSection.Contains("%FlyingTime%")) ? _itineraryHtmlSection.Replace("%FlyingTime%", item.) : "";

                        listItineraryHtmlSection.Add(_itineraryHtmlSection);
                    }
                    var _totalItinerary = string.Join("\r", listItineraryHtmlSection);
                    _itinerraryTemplate = _itinerraryTemplate.Replace(itineraryHtmlSection, _totalItinerary);
                    _itinerraryTemplate = _itinerraryTemplate.Replace("%Price%", offer.lblEmailTotPrice).Replace("%Currency%", offer.tdEmailCurrencyCode);

                    tableList.Add(_itinerraryTemplate);
                }
                tepmplateAfterParse = string.Join("<br><br>", tableList);
            }
            var _template = readHtmlTemplate.Replace(bodyHTML, tepmplateAfterParse);
            _template = _template.Replace("[template]", "")
                                 .Replace("[/template]", "")
                                 .Replace("[itinerary]", "")
                                 .Replace("[/itinerary]", "");
            return _template;
        }

        public static string Between(string STR, string FirstString, string LastString)
        {
            string FinalString;
            int Pos1 = STR.IndexOf(FirstString) + FirstString.Length;
            int Pos2 = STR.IndexOf(LastString);
            FinalString = STR.Substring(Pos1, Pos2 - Pos1);
            return FinalString;
        }

        private static List<string> FilterHtmlSections(string[] htmlSection)
        {
            List<string> htmlSectionFiltered = new List<string>();

            foreach (var section in htmlSection)
            {
                if (section.Contains("tdEmailAirline"))
                {
                    htmlSectionFiltered.Add(section);
                }
            }
            return htmlSectionFiltered;
        }

        private static Model htmlParse(HtmlDocument html_Section)
        {
            Model priceSection = new Model();

            var _price = string.Empty;
            var _currency = string.Empty;

            if (html_Section.DocumentNode.InnerHtml.Contains("lblEmailTotPrice"))
            {
                _price = (html_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'lblEmailTotPrice')]") != null) ? html_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'lblEmailTotPrice')]").InnerText : "";
                priceSection.lblEmailTotPrice = _price;

                _currency = (html_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'tdEmailCurrencyCode')]") != null) ? html_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'tdEmailCurrencyCode')]").InnerText : "";
                priceSection.tdEmailCurrencyCode = _currency.Replace("\r\n\t\t\t\t\t\t ", "");
            }
                        
            var _section = html_Section.DocumentNode.InnerHtml.Split(new string[] { "<td align=\"center\" id=\"tdEmailAirline\"" }, StringSplitOptions.RemoveEmptyEntries);

            //var listAfterSplit = SplitMyHtmlBy(html_Section.DocumentNode, "tdEmailAirline", "td");
            //List<string> contentInHtml = changeHtmlNodeToListString(listAfterSplit);

            if (_section.Count() > 0)
            {
                foreach (var item in _section)
                {
                    HtmlDocument tr_Section = new HtmlDocument();
                    tr_Section.LoadHtml(item);

                    if (tr_Section.DocumentNode.InnerHtml.Contains("tdEmailAirlineCode"))
                    {
                        var tdEmailAirline1 = string.Empty;

                        if (tr_Section.DocumentNode.InnerHtml.Contains("tdEmailAirline"))
                        {
                            var imageNode = tr_Section.DocumentNode.SelectSingleNode(".//img").Attributes.FirstOrDefault(o => o.Name == "src").Value;
                            if (!string.IsNullOrEmpty(imageNode))
                            {
                                var scrImage = string.Format("https://gopublic.wspan.com{0}", imageNode.ToString());
                                tdEmailAirline1 = scrImage;
                            }
                        }

                        var tdEmailAirlineCode1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailAirlineCode')]");
                        var lblEmailFlightNumber1 = tr_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'lblEmailFlightNumber')]");
                        var tdEmailBookingCode1 = tr_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'tdEmailBookingCode')]");

                        var tdEmailDepartureDate1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailDepartureDate')]");
                        var lblEmailDepartureCity1 = tr_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'lblEmailDepartureCity')]");
                        var tdEmailDepartureTime1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailDepartureTime')]");
                        var tdEmailArrivalTime1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailArrivalTime')]");
                        var lblEmailArrivalCity1 = tr_Section.DocumentNode.SelectSingleNode("//label[contains(@id,'lblEmailArrivalCity')]");
                        var tdEmailEquipCode1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailEquipCode')]");
                        var tdEmailStops1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailStops')]");
                        var tdEmailOperatedBy1 = tr_Section.DocumentNode.SelectSingleNode("//td[contains(@id,'tdEmailOperatedBy')]");
                        

                        string tdEmailAirline2 = (tdEmailAirline1 != null) ? tdEmailAirline1 : "";
                        string tdEmailAirlineCode2 = (tdEmailAirlineCode1 != null) ? tdEmailAirlineCode1.InnerText.ClearString().Trim() : "";
                        string lblEmailFlightNumber2 = (lblEmailFlightNumber1 != null) ? lblEmailFlightNumber1.InnerText : "";
                        string tdEmailDepartureDate2 = (tdEmailDepartureDate1 != null) ? tdEmailDepartureDate1.InnerText.ClearString().Trim() : "";
                        string tdEmailBookingCode2 = (tdEmailBookingCode1 != null) ? tdEmailBookingCode1.InnerText : "";
                        string lblEmailDepartureCity2 = (lblEmailDepartureCity1 != null) ? lblEmailDepartureCity1.InnerText.ClearString() : "";
                        lblEmailDepartureCity2 = RearrangeData(lblEmailDepartureCity2);
                        string tdEmailDepartureTime2 = (tdEmailDepartureTime1 != null) ? tdEmailDepartureTime1.InnerText : "";
                        string tdEmailArrivalTime2 = (tdEmailArrivalTime1 != null) ? tdEmailArrivalTime1.InnerText.ClearString() : "";
                        string lblEmailArrivalCity2 = (lblEmailArrivalCity1 != null) ? lblEmailArrivalCity1.InnerText.ClearString() : "";
                        lblEmailArrivalCity2 = RearrangeData(lblEmailArrivalCity2);
                        string tdEmailEquipCode2 = (tdEmailEquipCode1 != null) ? tdEmailEquipCode1.InnerText.ClearString()
                                                                                                            .Replace("Equipment :", "").Trim() : "";
                        string tdEmailStops2 = (tdEmailStops1 != null) ? tdEmailStops1.InnerText.ClearString()
                                                                                                .Replace("Stops:", "").Trim() : "";
                        string tdEmailOperatedBy2 = string.Empty;
                        if (tdEmailOperatedBy1 != null)
                        {
                            if (tdEmailOperatedBy1.InnerText.Contains("Operated By :"))
                            {
                                var _string = tdEmailOperatedBy1.InnerText.Replace("Operated By :", "").Trim();
                                tdEmailOperatedBy2 = _string;
                            }
                        }                        

                         PropModel _propModel = new PropModel
                        {
                            tdEmailAirline = tdEmailAirline2,
                            tdEmailAirlineCode = tdEmailAirlineCode2,
                            lblEmailFlightNumber = lblEmailFlightNumber2,
                            tdEmailBookingCode = tdEmailBookingCode2,
                            tdEmailDepartureDate = tdEmailDepartureDate2,
                            lblEmailDepartureCity = lblEmailDepartureCity2,
                            tdEmailDepartureTime = tdEmailDepartureTime2,
                            tdEmailArrivalTime = tdEmailArrivalTime2,
                            lblEmailArrivalCity = lblEmailArrivalCity2,
                            tdEmailEquipCode = tdEmailEquipCode2,
                            tdEmailStops = tdEmailStops2,
                            tdEmailOperatedBy = tdEmailOperatedBy2
                        };
                        priceSection.propModel.Add(_propModel);
                    }
                }
            }
            return priceSection;
        }

        private static string RearrangeData(string _airportData)
        {
            string airportData = _airportData;
            try
            {
                if (!string.IsNullOrEmpty(airportData))
                {
                    var trySplitData = airportData.Split(',').Select(s => s.Trim()).ToList();
                    var trySwapIntex = trySplitData.SwapIndex(1, 0);
                    airportData = string.Join(", ", trySwapIntex);
                }                                               
            }
            catch (Exception e)
            {
                return airportData;
            }
            return airportData;
        }

       
    }
}