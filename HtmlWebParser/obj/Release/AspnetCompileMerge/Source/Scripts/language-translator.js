﻿$(function () {

    // Lets be professional, shall we?
    "use strict";

    // Some variables for later
    var dictionary, set_lang;

    // Object literal behaving as multi-dictionary
    dictionary = {
        "english": {
            "_Option": "Option",
            "_Airline": "Airline",
            "_Flight_Number": "Flight Number",
            "_Class": "Class",
            "_From": "From",
            "_Destination": "Destination",
            "_Leaving": "Leaving",
            "_Arriving": "Arriving",
            "_Aircraft_Type": "Aircraft Type",
            "_Operated_By": "Operated By",
            "_Stops": "Stops",
            "_Total": "Total",
            "_Cancellation_Policy": "Cancellation Policy",
            "_Changes": "Changes",
            "_Notes": "Notes"
        },
        "polish": {
            "_Option": "Opcja",
            "_Airline": "Linia",
            "_Flight_Number": "Numer lotu",
            "_Class": "Klasa",
            "_From": "Z",
            "_Destination": "Do",
            "_Leaving": "Wylot",
            "_Arriving": "Przylot",
            "_Aircraft_Type": "Samolot",
            "_Operated_By": "Obsługiwany przez",
            "_Stops": "Przesiadki",
            "_Total": "Cena",
            "_Cancellation_Policy": "Warunki zwrotu biletu",
            "_Changes": "Warunki zmiany biletu",
            "_Notes": "Uwagi"
        },
        "romanian": {
            "_Option": "Varianta",
            "_Airline": "Companie Aeriana",
            "_Flight_Number": "Numarul zborului",
            "_Class": "Clasa",
            "_From": "De la",
            "_Destination": "Destinatie",
            "_Leaving": "Plecare",
            "_Arriving": "Sosire",
            "_Aircraft_Type": "Aeronava",
            "_Operated_By": "Operat de",
            "_Stops": "Escala",
            "_Total": "Total",
            "_Cancellation_Policy": "Politica de anulare",
            "_Changes": "Modificari",
            "_Notes": "Note"
        }
    };

    // Function for swapping dictionaries
    set_lang = function (dictionary) {
        $("[data-translate]").text(function () {
            var key = $(this).data("translate");
            if (dictionary.hasOwnProperty(key)) {
                return dictionary[key];
            }
        });
    };

    // Swap languages when menu changes
    $("#lang").on("change", function () {
        var language = $(this).val().toLowerCase();
        if (dictionary.hasOwnProperty(language)) {
            set_lang(dictionary[language]);
        }
    });

    // Set initial language to English
    set_lang(dictionary.english);

});
