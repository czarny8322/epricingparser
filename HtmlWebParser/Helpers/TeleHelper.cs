﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelemetrySdk.Helpers;

namespace HtmlWebParser.Helpers
{
    static class TeleHelper
    {
        public static string telemetryClientId = "ePricingParser";

        public static void LogCustomObject(object obj)
        {
            TelemetryHelper.Instance.LogCustomObject(telemetryClientId, obj);
        }
        public static void LogException(string exception)
        {
            TelemetryHelper.Instance.LogException(telemetryClientId, exception);
        }

    }
}